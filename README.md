# Base
Base image with apache and php.  
It is used for building the development and production apache images, as well as the builder image.

The docker repository can be found [here](https://hub.docker.com/r/pricepilotio/base/).
